import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProductDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-detail',
  templateUrl: 'product-detail.html',
})
export class ProductDetailPage {
  products = [];
  relproducts = [];
  productCount: number = 1;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.products = [
      {
        name: "Amritsari Chicken Masala Recipe",
        image: "a.png",
        price:"200",
        id:"1",
      }
    ];

    this.relproducts = [
      {
        name: "Chicken Chettinad",
        image: "e.png",
        price:"600",
        id:"5",
      },
      {
        name: "Spicy Tangy Kadhai Chicken",
        image: "f.png",
        price:"700",
        id:"6",
      }
     
    ];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetailPage');
  }
  decreaseproductcount()
  {
    if(this.productCount>1)
    {
      this.productCount--;
    }

  }
  incrementproductcount()
  {
    this.productCount++;
  }

}
