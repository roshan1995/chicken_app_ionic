import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProductDetailPage } from '../product-detail/product-detail';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  type_tab:any = 'chicken';
  products = [];
  constructor(public navCtrl: NavController) {
    this.products = [
      {
        name: "Amritsari Chicken Masala Recipe",
        image: "a.png",
        price:"200",
        id:"1",
      },
      {
        name: "Teekha Murg",
        image: "b.png",
        price:"300",
        id:"2",
      },
      {
        name: "Murg Malaiwala",
        image: "c.png",
        price:"400",
        id:"3",
      },
      {
        name: "Kerala Chicken Roast",
        image: "d.png",
        price:"500",
        id:"4",
      },
      {
        name: "Chicken Chettinad",
        image: "e.png",
        price:"600",
        id:"5",
      },
      {
        name: "Spicy Tangy Kadhai Chicken",
        image: "f.png",
        price:"700",
        id:"6",
      }
     
    ];


  }
  pushPage(id)
  {
    this.navCtrl.push(ProductDetailPage, {
      id: id
    });
  }

}
